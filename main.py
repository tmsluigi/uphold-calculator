from sys import argv
import requests

TICKER_API_URL = "https://api.uphold.com/v0/ticker/"


def process_line_portfolio(transaction, funds, differences):
    """
    This processes one line from the csv file
    :param transaction: A line from the csv file
    :param funds: The dictionary where funds will be added or updated
    :param differences: Showing what transactions made up this current final amount
    :return: None, just updates funds
    """
    # Get the money in proper units
    transaction = transaction.split(",")
    destination_amount = float(transaction[2])
    destination_currency = transaction[3]
    origin_amount = float(transaction[8])
    origin_currency = transaction[9]
    # Update the portfolio
    funds[destination_currency] = funds.get(destination_currency, 0.0) + destination_amount
    funds[origin_currency] = funds.get(origin_currency, 0.0) - origin_amount
    # Figure out what the differences in the exchange is
    if destination_currency not in differences.keys():
        differences[destination_currency] = {}
    if origin_currency not in differences.keys():
        differences[origin_currency] = {}
    differences[destination_currency][origin_currency] = differences[destination_currency].get(
        origin_currency, 0.0) + origin_amount
    differences[origin_currency][destination_currency] = differences[origin_currency].get(
        destination_currency, 0.0) - destination_amount


def get_exchange_rates(asset):
    """
    Get exchange rates based on the asset three letter name passed in
    :param asset: Three letter name to get asset, such as "USD"
    :return: A list of dictionaries containing exchange rates
    """
    response = requests.get(TICKER_API_URL + asset)
    response_json = response.json()
    return response_json


def get_exchange_rate(rates):
    """
    Get the exchange rate from the json object from uphold
    :param rates: The uphold rate from the request
    :return: a double the converts usd to the rate in the dictionary
    """
    return (float(rates["ask"]) + float(rates["bid"])) / 2.0


def get_usd_portfolio(portfolio_local):
    """
    Get the USD value of everything in the portfolio and return that
    :param portfolio_local: The portfolio to convert to USD
    :return: A portfolio just in USD
    """
    usd_portfolio_local = {}
    for key, value in portfolio_local.items():
        multiply_rate = get_conversion_rate_from_ticker(key)
        usd_portfolio_local[key] = multiply_rate * portfolio_local[key]
    return usd_portfolio_local


def get_conversion_rate_from_ticker(ticker_symbol):
    """
    Given a ticker, get the conversion ratio to USD
    :param ticker_symbol: The ticker symbol
    :return: The conversion rate
    """
    if ticker_symbol == 'USD':
        multiply_rate = 1.0
    else:
        rates = list(filter(lambda x: x["pair"][0:3] == ticker_symbol, usd_exchange_rates))[0]
        multiply_rate = get_exchange_rate(rates)
    return multiply_rate


def get_usd_return(usd_portfolio_local):
    """
    Add everything in the USD portfolio up
    :param usd_portfolio_local: The portfolio to add up
    :return: Return the portfolio amount
    """
    total = 0.0
    for k in usd_portfolio_local.values():
        total += k
    return total


def get_percent_return(usd_portfolio_local):
    """
    Get the percent return, which is all positive values over all negative values
    :param usd_portfolio_local: The just USD portfolio
    :return: total positive / total negative
    """
    negative_total = 0.0
    positive_total = 0.0
    # Sum up the negatives and positives
    for value in usd_portfolio_local.values():
        if value < 0.0:
            negative_total += value
        else:
            positive_total += value
    # Return the ratio
    return positive_total / abs(negative_total) * 100


def get_individual_buy_price(differences):
    """
    Gets how much money (USD) was put into each asset using today's exchange rate
    :param differences: A dictionary from the get differences function
    :return: A dictionary that maps currency to USD invested in that currency
    """
    invest_yield = {}
    for destination_currency, composite in differences.items():
        total_usd = 0
        for origin_currency, value in composite.items():
            total_usd += value * get_conversion_rate_from_ticker(origin_currency)
        invest_yield[destination_currency] = total_usd
    return invest_yield


def get_individual_return(usd_portfolio, individual_buy_price):
    """
    Gets the absolute return and percent return for each crypto invested
    :param usd_portfolio: Portfolio of crypto to USD
    :param individual_buy_price: How much each asset was bought in USD
    :return: The absolute return, and the percent return
    """
    returns_abs = {}
    returns_percent = {}
    for ticker, usd_value in usd_portfolio.items():
        if usd_value < 0:
            continue
        returns_abs[ticker] = usd_value - individual_buy_price[ticker]
        returns_percent[ticker] = usd_value / individual_buy_price[ticker] * 100
    return returns_abs, returns_percent


if __name__ == "__main__":
    # Find the value of the investments given an uphold csv transaction file
    if len(argv) < 2:
        raise ValueError("Need the file to process")
    portfolio = {}
    differences_main = {}
    # Process all the lines
    with open(argv[1]) as input_csv:
        for i, line in enumerate(input_csv):
            if i == 0:
                continue
            process_line_portfolio(line, portfolio, differences_main)

    # Get the exchange rates from uphold
    usd_exchange_rates = get_exchange_rates("USD")
    # Get the value of the assets in just USD
    usd_portfolio_main = get_usd_portfolio(portfolio)
    # Get individual return
    individual_buy_price_main = get_individual_buy_price(differences_main)
    # Get individual return
    individual_abs, individual_percent = get_individual_return(usd_portfolio_main, individual_buy_price_main)
    # Print out some information
    print(f"Portfolio USD: {usd_portfolio_main}")
    print(f"Portfolio: {portfolio}")
    print(f"Individual returns: {individual_abs}")
    print(f"Individual returns percent: {individual_percent}")
    print(f"The absolute return: ${get_usd_return(usd_portfolio_main):.2f}")
    print(f"The percent return: {get_percent_return(usd_portfolio_main):.2f}%")
